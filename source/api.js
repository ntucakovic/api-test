const defaultOptions = {
  method: "GET",
  redirect: "follow"
}

export default class Api {
  static fetch (url, options) {
    options = {
      ...defaultOptions,
      ...options
    }

    return fetch(url, options).then(response => response.json());
  }
}
