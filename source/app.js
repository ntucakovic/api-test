import Course from "../components/Course.js"
import { PageWrapper } from "../components/PageWrapper.js"
import Api from "./api.js";
import { API_ENDPOINTS } from "./constants.js";

export class App {
  constructor () {
    this.getMarkup.bind(this);
    this.initializeCourses.bind(this);

    this.initializeCourses()
      .then(() => {
        const markup = this.getMarkup();
        const pageWrapper = new PageWrapper(markup);
        document.getElementsByTagName("body")[0].innerHTML = pageWrapper.render();
    });
  }

  static getData () {
    return new Promise(resolve => {
      Api.fetch(API_ENDPOINTS.courses)
        .then(response => {
          if (response) {
            resolve(response);
          }
      })
    });
  }

  initializeCourses () {
    return App.getData()
      .then(response => {
        const { courses = [] } = response || {};
        this.courses = courses.map(course => {
          return new Course(course);
        });
    });
  }

  getMarkup () {
    const coursesMarkup = this.courses.map(course => {
      return course.render();
    });

    return coursesMarkup.join("\n");
  }
}

export default new App();
