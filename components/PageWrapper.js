export class PageWrapper {
  constructor (innerHTML) {
    this.innerHTML = innerHTML;
  }

  render () {
    return `
      <div class="row">
         <div class="column">
            ${this.innerHTML || ""}
         </div>
      </div>
    `
  }
}
