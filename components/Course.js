const defaultImage = "https://via.placeholder.com/150x100";

export default class Course {
  constructor ({ title = null, summary = null, image = null, homepage = null }) {
    this.title = title;
    this.summary = summary;
    this.homepage = homepage;
    this.image = image || defaultImage;
  }

  render () {
    return `
      <div class="course">
        <div class="course__image">
          <img src="${ this.image }" alt="${this.title}" class="course__image">  
        </div>
        <div class="course__details">
          <h2 class="course__title">
            ${this.homepage !== null ? `
              <a href="${this.homepage}" target="_blank" rel="noopener noreferrer">
                  ${this.title || ""}
              </a>
            ` :
              this.title || ""
            }
          </h2>
          <div class="course__summary">
            <p>${this.summary || ""}</p>
          </div>
        </div>
      </div>
    `
  }
}
